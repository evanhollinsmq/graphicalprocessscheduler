import processing.core.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main extends PApplet {

    private static final Logger LOGGER = Logger.getLogger(Main.class.getName());

    private final static int TASK_BOX_HEIGHT = 20;
    private final static int TASK_TOTAL_HEIGHT = TASK_BOX_HEIGHT * 2;
    private final static int SECONDS_TO_SIMULATE = 15;
    private final static int TIME_STEP = 1;
    private final static int PADDING = 10;
    private final static int TEXT_PADDING = 50;
    private final static int TOTAL_PADDING = PADDING + TEXT_PADDING;
    private final static int TIME_DIVIDER_WIDTH = 5;
    private final int TIME_DIVIDER_COLOR = color(50, 200, 250);

    private enum METHOD {
        RM,
        EDF;
    }
    private ArrayList<Task> tasks;
    private METHOD currentMethod;
    private int secondToPixel;


    private class Task {
        private int period;
        private int executionTime;
        private int yLocation;
        private double utilisation;
        private int color;
        private double priority;
        private int amountExecuted;

        public Task(int period, int executionTime, int color) {
            this.period = period;
            this.executionTime = executionTime;
            this.color = color;
            this.yLocation = -1;
            this.amountExecuted = 0;

            this.utilisation = (double) this.executionTime / this.period;
        }

        public void drawLine() {
            if(yLocation < 0) {
                LOGGER.log(Level.SEVERE, "Y Locations not set. Please Call setYLocations after adding all tasks.");
                return;
            }

            strokeWeight(4);
            stroke(0);
            line(PADDING, yLocation, width - PADDING, yLocation);
        }

        public void drawMarkers() {
            if(yLocation < 0) {
                LOGGER.log(Level.SEVERE, "Y Locations not set. Please Call setYLocations after adding all tasks.");
                return;
            }

            stroke(color);
            strokeWeight(2);

            for(int i = 0; i <= SECONDS_TO_SIMULATE; i += period) {
                line(i * secondToPixel + TOTAL_PADDING,
                        yLocation,
                        i * secondToPixel + TOTAL_PADDING,
                        yLocation - (TASK_BOX_HEIGHT + TASK_BOX_HEIGHT /4)
                        );
            }
        }

        public void drawSignature() {
            if(yLocation < 0) {
                LOGGER.log(Level.SEVERE, "Y Locations not set. Please Call setYLocations after adding all tasks.");
                return;
            }

            fill(color);
            text(String.format("(%d, %d)", this.period, this.executionTime),
                    PADDING + TEXT_PADDING/2,
                    yLocation - 10);
        }

        public void draw(int time) {
            if(yLocation < 0) {
                LOGGER.log(Level.SEVERE, "Y Locations not set. Please Call setYLocations after adding all tasks.");
                return;
            }

            fill(color);
            noStroke();
            rect(TOTAL_PADDING + time * secondToPixel,
                    yLocation - TASK_BOX_HEIGHT,
                    executionTime * secondToPixel,
                    TASK_BOX_HEIGHT);
        }

        public void draw(int time, int executionTime) {
            if(yLocation < 0) {
                LOGGER.log(Level.SEVERE, "Y Locations not set. Please Call setYLocations after adding all tasks.");
                return;
            }

            fill(color);
            noStroke();
            rect(TOTAL_PADDING + time * secondToPixel,
                    yLocation - TASK_BOX_HEIGHT,
                    executionTime * secondToPixel,
                    TASK_BOX_HEIGHT);
        }
    }

    private void rateMonotonic() {
        PriorityQueue<Task> queue = new PriorityQueue<>(tasks.size(), new Comparator<Task>() {
            @Override
            public int compare(Task o1, Task o2) {
                if(o1.priority > o2.priority)
                    return 1;
                else if(o1.priority < o2.priority) {
                    return -1;
                }
                return 0;
            }
        });

        Task currentTask;

        for(Task t: tasks) {
            t.priority = t.period;
            queue.add(t);
        }

        currentTask = queue.peek();
        currentTask.draw(0, 1);
        currentTask.amountExecuted++;
        if(currentTask.amountExecuted == currentTask.executionTime) {
            currentTask.amountExecuted = 0;
            queue.poll();
        }

        for(int currentTime = TIME_STEP; currentTime < SECONDS_TO_SIMULATE; currentTime += TIME_STEP) {
            for(Task t: tasks) {
                if(currentTime % t.period == 0) {
                    if(queue.contains(t)) {
                        t.amountExecuted = 0;
                    }
                    else {
                        queue.add(t);
                    }
                }
            }

            if(queue.size() > 0) {
                currentTask = queue.peek();
                currentTask.draw(currentTime, 1);
                currentTask.amountExecuted++;
                if(currentTask.amountExecuted == currentTask.executionTime) {
                    currentTask.amountExecuted = 0;
                    queue.poll();
                }

            }
        }
    }

    private boolean rateMonotonicPossible() {
        double sum = 0;
        int n = tasks.size();
        for(Task t: tasks) {
            sum += t.utilisation;
        }
        return sum <= n * (Math.pow(2, 1/n) - 1);
    }

    private void earliestDeadlineFirst() {
        PriorityQueue<Task> queue = new PriorityQueue<>(tasks.size(), new Comparator<Task>() {
            @Override
            public int compare(Task o1, Task o2) {
                if(o1.priority > o2.priority)
                    return 1;
                else if(o1.priority < o2.priority) {
                    return -1;
                }
                return 0;
            }
        });

        Task currentTask;

        for(int currentTime = 0; currentTime < SECONDS_TO_SIMULATE; currentTime += TIME_STEP) {
            queue.clear();
            for(Task t: tasks) {
                if(currentTime % t.period == 0) {
                    t.priority = t.period;
                    t.amountExecuted = 0;
                }
                else {
                    t.priority = t.period - (currentTime % t.period);
                }
                if(t.amountExecuted != t.executionTime) {
                    queue.add(t);
                }
            }

            if(queue.size() > 0) {
                currentTask = queue.poll();
                currentTask.draw(currentTime, 1);
                currentTask.amountExecuted++;
            }
        }
    }

    private boolean earliestDeadlineFirstPossible() {
        double sum = 0;
        int n = tasks.size();
        for(Task t: tasks) {
            sum += t.utilisation;
        }
        return sum <= 1;
    }

    private void setYLocations() {
        if(tasks.size() > (height - TOTAL_PADDING * 2) / TASK_TOTAL_HEIGHT) {
            LOGGER.log(Level.SEVERE, "Too many tasks based on task total height and window height.");
            exit();
        }

        int i = (height - TASK_TOTAL_HEIGHT * tasks.size()) / 2 + TASK_TOTAL_HEIGHT;
        for(Task t: tasks) {
            t.yLocation = i;
            i += TASK_TOTAL_HEIGHT;
        }
    }


    private void drawFramwork() {
        for(int i = 0; i <= SECONDS_TO_SIMULATE; i += TIME_DIVIDER_WIDTH) {
            strokeWeight(1);
            stroke(TIME_DIVIDER_COLOR);
            line(TOTAL_PADDING + (i * secondToPixel),
                    tasks.get(tasks.size() - 1).yLocation,
                    TOTAL_PADDING + (i * secondToPixel),
                    tasks.get(0).yLocation - TASK_TOTAL_HEIGHT - PADDING);

            fill(0);
            text(i, TOTAL_PADDING + (i * secondToPixel), tasks.get(tasks.size() - 1).yLocation + 20);
        }
    }

    public static void main(String[] args) {
        PApplet.main("Main");
    }

    @Override
    public void settings() {
        size(720, 405);

        tasks = new ArrayList<>();
        currentMethod = METHOD.EDF;

        tasks.add(new Task(6, 2, color(255, 150, 60)));
        tasks.add(new Task(4, 1, color(0, 255, 0)));
        tasks.add(new Task(3, 1, color(255, 0, 0)));

        setYLocations();


    }

    @Override
    public void setup() {
        background(255);

        // Setup Text
        textSize(12);
        textAlign(CENTER);
        fill(0);

        secondToPixel = (width - TOTAL_PADDING * 2) / SECONDS_TO_SIMULATE;

        drawFramwork();

        for(Task t: tasks) {
            t.drawLine();
            t.drawMarkers();
            t.drawSignature();
        }

        switch(currentMethod) {
            case RM:
                rateMonotonic();
                System.out.println(rateMonotonicPossible());
                break;
            case EDF:
                earliestDeadlineFirst();
                System.out.println(earliestDeadlineFirstPossible());
                break;
        }


    }

    @Override
    public void draw() {

    }
}
